#!/usr/bin/python

import psycopg2
import math
from ipaddr import IPAddress, IPNetwork, IPv4Address, IPv4Network, IPv6Address, IPv6Network;
from socket import getaddrinfo, AF_INET, AF_INET6;
from subprocess import PIPE;

import time;
import socket;
import os;
import re;
import sys;
import errno;
import codecs;
import datetime;
#import crypt;
import random;
import string;
import subprocess



###################################################################################################
###################### CREATE Database ############################################################

def create(table_route) :
    table = table_route
    try: 
        sql = '''CREATE TABLE ''' + str(table) + '''
        (Date TIMESTAMP NOT NULL,
        Version INT  NOT NULL,
        Fromsi    INT    NOT NULL,
        Frompi   INT     NOT NULL,
        FromIP    INET  NOT NULL, 
        Tosi    INT    NOT NULL,  
        TOpi   INT     NOT NULL,  
        TOIP   INET  NOT NULL,     
        Path     TEXT  NOT NULL,   
        Pathid   TEXT   NOT NULL,  
        Hopnumber INT  NOT NULL,   
        Pingnumber  INT   NOT NULL,
        Min    TEXT   NOT NULL, 
        Avg     TEXT  NOT NULL, 
        Max     TEXT  NOT NULL,
        Std     TEXT   NOT NULL,
        Scheme    TEXT,
        Totalmin   REAL  NOT NULL,
        Totalavg   REAL  NOT NULL,
        Totalmax   REAL  NOT NULL,
        Totalstd   REAL  NOT NULL); '''
        
        cur.execute(sql)
    
        print "Table created successfully"
    except:
        print "can not create the table!."
    conn.commit()
##################################################################################################################################
################# INSERT #########################################################################################################

def insert(result_path,table_route):
    pattern = ".*route4.*"
    pattern2 = ".*route6.*"
    regexp = re.compile(pattern)
    regexp2 = re.compile(pattern2)
    path_file = result_path
    table = table_route
    if (os.path.exists(path_file)):
        list = os.listdir(path_file)
        for data in list:
            filename = path_file + "/" + str(data)
            i = 0
            result = regexp.search(data)
            result2 = regexp2.search(data)
            if result :     
                version = 4
            if result2:
                version = 6
            file = open( filename, "r")
            for line in file :
                line = str(line)
                date = line.split(",")[0]
                date= datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S.%f')
                fsi = int(line.split(",")[1])
                fpi = int(line.split(",")[2])
                #fIP = IPAddress(line.split(",")[3])
                fIP = line.split(",")[3]
                tsi = int(line.split(",")[4])
                tpi = int(line.split(",")[5])
               # tIP = IPAddress(line.split(",")[6])
                tIP = line.split(",")[6]
                path = line.split(",")[7]
                pathid = line.split(",")[8]
                hop_no = int(line.split(",")[9])	
                ping_no = int(line.split(",")[10])
                min = line.split(",")[11]
                avg = line.split(",")[12]
                max = line.split(",")[13]
                std = line.split(",")[14]
                scheme = line.split(",")[15]
                totalmin = float(line.split(",")[16])
                totalavg = float(line.split(",")[17])
                totalmax = float(line.split(",")[18])
                totalstd = float(line.split(",")[19])
                try:
                    sql = "INSERT INTO " + table + "(Date,Version,Fromsi,Frompi,FromIP,Tosi,TOpi,TOIP,Path,Pathid,Hopnumber,Pingnumber,Min,Avg,Max,Std,Scheme,Totalmin,Totalavg,Totalmax,Totalstd) VALUES(%s, %s, %s, %s, %s, %s, %s, %s,  %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s); "
                    cur.execute(sql, (date,version,fsi,fpi,fIP,tsi,tpi,tIP,path,pathid,hop_no,ping_no,min,avg,max,std,scheme,totalmin,totalavg,totalmax,totalstd))
                    conn.commit()
                except:
                #    print "I am not be able to insert to the database!"
                    sys.exit()
                # print "Records inserted successfully";
            file.close()
            os.remove(filename)    
#####################################################################################################
################################### DELETE ##########################################################                      
def delete(table_route):
    table = table_route
    try:
        sql = "DELETE from " + str(table) + "*"
        cur.execute(sql)
        conn.commit()
        print "Total number of rows deleted :", cur.rowcount
    except:
        print "can nor delete anythind!."


#####################################################################################################
####################### SELECT ######################################################################
def select(table_route):
    table = table_route
    try:
        sql = "SELECT *  from " + str(table)
        print sql
        cur.execute(sql)
        rows = cur.fetchall()
    except:
        print "I am not be able to fetch data from the database!"
        return -1
    for row in rows:
        print "Date = ", row[0]
        print "From Address = ", row[4]
       # ip = IPNetwork(row[4]).ip
        #print type(ip)
        print "To Address = ", row[7]
        print "Path is:" , row[8]
        print "Pathid = ", row[9]
        print "Number of Hops= ", row[10]
        hop = row[10]
    print "Operation done successfully";

#######################################################################################################
#######################################################################################################
################### DROP TABLE ##############################################################################

def drop(table_route):
    table = table_route
    conn.set_isolation_level(0)
    try:
        sql = "DROP TABLE " + str(table)
        cur.execute(sql)
    except:
        print "I can't drop our test database!"
    conn.commit()
    #conn.set_isolation_level(0)
   # cur.execute ("DROP TABLE route4")

#####################################################################################################
#####################################################################################################
####################### MAIN #######################################################################
conf_path = "/etc/nornet/Trace-Configuration"
conf_file = open(conf_path,'r')
for line in conf_file:
    name = line.split(" ")[0]
    print name
    if (name == 'result_path'):
        result_path = (line.split(" ")[2]).rstrip()
    elif (name == 'dbname'):
        database_name = (line.split(" ")[2]).rstrip()
    elif (name == 'user'):
        user_name = (line.split(" ")[2]).rstrip()
    elif (name == 'host'):
        host_name = (line.split(" ")[2]).rstrip()
    elif (name == 'port'):
        port_no = (line.split(" ")[2]).rstrip()
    elif (name == 'password'):
        pass_phrase = (line.split(" ")[2]).rstrip()
    elif (name == 'table1'):
        table_route = (line.split(" ")[2]).rstrip()
try:
    conn = psycopg2.connect(dbname=str(database_name), user=str(user_name), host=str(host_name), port=str(port_no), password=str(pass_phrase))
except:
    print "I am not be able to connect to the database!"
    sys.exit()
#print "Opened database successfully"
cur = conn.cursor()
#create(table_route)
insert(result_path,table_route)
#select(table_route)
#delete(table_route)
#drop(table_route)
conn.commit()
conn.close()

