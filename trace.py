#!/usr/bin/python

from ipaddr import IPAddress, IPNetwork, IPv4Address, IPv4Network, IPv6Address, IPv6Network;
from socket import getaddrinfo, AF_INET, AF_INET6;
from subprocess import PIPE;
from subprocess import call;

import math;
import time;
import socket;
import os;
import re;
import sys;
import errno;
import codecs;
import datetime;
import random;
import string;
import subprocess
# NorNet
from NorNetTools         import *;
from NorNetConfiguration import *;
from NorNetAPI           import *;
from NorNetNodeSetup     import *;
import hashlib
############################################################################################
####################  Traceroute ###########################################################
#####################################  Traceroute4 #########################################
def traceroute4(local,remote,localsiteindex,localproviderindex,remotesiteindex,remoteproviderindex,filename) :
#    print local
#    print remote
    hopslist = []
    path = ""
    min = ""
    avg = ""
    max = ""
    std = ""
    ping_num = 3
    scheme = ""
    totalmin = ""
    totalavg = ""
    totalmax = ""
    totalstd = ""
    pattern = ".*\s+icmp_seq=\d+\s+Time\sto\slive\sexceeded"
    pattern2 = "64.*icmp_req=\d+.*\s+ms"
    regexp = re.compile(pattern)
    regexp2 = re.compile(pattern2)
    i = 1
    timest = datetime.datetime.now()
    while (i < 35 ):
        cm = "ping -w 2 -c 1 -t " + str(i) + " -I " +str(local) + " " + str(remote)
        ping = subprocess.Popen(cm , stdout = PIPE, shell = True)
        line  = ping.stdout.readlines()
        result = regexp.search(line[1])
        result2 = regexp2.search(line[1])
        if result2:
           # print "XXXXXXXXXXX"
           # print "The number of hops between is :" + str(i)
            hopslist.append(str(remote))
            break
        elif result:
            hopslist.append(line[1].split(" ")[1])
            i = i+1
        else :
            hopslist.append('*')
            i = i+1
    hop_num = i
    time = timest.time()
    filename = filename + '-' +  str(time)
    file = open(filename,"w")
    for i in hopslist:
        if (i != '*'):
            cm2 = "ping -w " + str(ping_num) + " -c " + str(ping_num) + " -I " + str(local) + " " + str(i) + " | grep rtt"
            ping2 = subprocess.Popen(cm2 , stdout = PIPE, shell = True)
            out2 = ping2.communicate()[0]
            if str(out2) == '' :
                min = min + '*' + '-'
                avg = avg + '*' + '-'
                max = max + '*' + '-'
                std = std + '*' + '-'
                path = path + str(i) + '-'
            else :
	        out2 = str(out2)
	        data = out2.split(" ")[3]
	        min = min + str(data.split("/")[0])+ '-'
	        avg = avg + str(data.split("/")[1])+ '-'
	        max = max + str(data.split("/")[2])+ '-'
	        std = std + str(data.split("/")[3])+ '-'
	        path = path + str(i) + '-'
        else:
            min = min + '*' + '-'
            avg = avg + '*' + '-'
            max = max + '*' + '-'
            std = std + '*' + '-'
            totalmin = 0
            totalavg = 0
            totalmax = 0
            totalstd = 0
            path = path + str(i) + '-'
        if (str(i) == str(remote)):
            totalmin = str(data.split("/")[0])
            totalavg = str(data.split("/")[1])
            totalmax = str(data.split("/")[2])
            totalstd = str(data.split("/")[3])
    hash_path = hashlib.sha512(path)
    hash_hex = hash_path.hexdigest()
    file.write(str(timest) + ',' + str(localsiteindex) + ',' + str(localproviderindex) + ',' + str(local) + ',' + str(remotesiteindex) + ',' + str(remoteproviderindex) + ',' + str(remote) + ',' + path + ',' + str(hash_hex) + ',' + str(hop_num) + ',' + str(ping_num) + ',' + min + ',' + avg + ',' + max + ',' + std + ',' + scheme + ',' + str(totalmin) + ',' + str(totalavg) + ',' + str(totalmax) + ',' + str(totalstd) + '\n')
    file.close()

############################################################################################
####################  Traceroute6 ###########################################################
def traceroute6(local,remote,localsiteindex,localproviderindex,remotesiteindex,remoteproviderindex,filename) :
   # print local
   # print remote
    hopslist6 = []
    path = ""
    min = ""
    avg = ""
    max = ""
    std = ""   
    ping_num = 3
    scheme = ""
    totalmin = ""
    totalavg = ""
    totalmax = ""
    totalstd = ""
    pattern = ".*\s+icmp_seq=\d+\s+Time\sexceeded:.*"
    pattern2 = "64.*icmp_seq=\d+.*\s+ms"
    regexp = re.compile(pattern)
    regexp2 = re.compile(pattern2) 
    i = 1
    timest = datetime.datetime.now()
    while (i < 35 ):
        cm = "ping6 -w 2 -c 1 -t " + str(i) + " -I " +str(local) + " " + str(remote)
        ping = subprocess.Popen(cm , stdout = PIPE, shell = True)
        line  = ping.stdout.readlines()
        result = regexp.search(line[1])
        result2 = regexp2.search(line[1])
        if result2:
           # print "6666666"
           # print "The number of hops between is :" + str(i)
            hopslist6.append(str(remote))
            break
        if result :
            hopslist6.append(line[1].split(" ")[1])
            i = i+1
        else :
            hopslist6.append('*')
            i = i+1
                                
    hop_num = i
    time = timest.time()
    filename = filename + '-' + str(time)
    file6 = open(filename,"w")
    for i in hopslist6:
        if (i != '*'):
            cm26 = "ping6 -w " + str(ping_num) + " -c " + str(ping_num) + " -I " + str(local) + " " + str(i) + " | grep rtt"
            ping26 = subprocess.Popen(cm26 , stdout = PIPE, shell = True)
            out26 = ping26.communicate()[0]
            if str(out26) == '' :
                min = min + '*' + '-'
                avg = avg + '*' + '-'
                max = max + '*' + '-'
                std = std + '*' + '-'
                path = path + str(i) + '-'      
            else :
                out26 = str(out26)
                data6 = out26.split(" ")[3]
                min = min + str(data6.split("/")[0])+ '-'
                avg = avg + str(data6.split("/")[1])+ '-'
                max = max + str(data6.split("/")[2])+ '-'
                std = std + str(data6.split("/")[3])+ '-'
                path = path + str(i) + '-'
        else:
            min = min + '*' + '-'
            avg = avg + '*' + '-'
            max = max + '*' + '-'
            std = std + '*' + '-'
            totalmin = 0
            totalavg = 0
            totalmax = 0
            totalstd = 0
            path = path + str(i) + '-'
        if ( str(i) == str(remote) ):
            totalmin = str(data6.split("/")[0])
            totalavg = str(data6.split("/")[1])
            totalmax = str(data6.split("/")[2])
            totalstd = str(data6.split("/")[3])
    hash_path = hashlib.sha512(path)
    hash_hex = hash_path.hexdigest()
    file6.write(str(timest) + ',' + str(localsiteindex) + ',' + str(localproviderindex) + ',' + str(local) + ',' + str(remotesiteindex) + ',' + str(remoteproviderindex) + ',' + str(remote) + ','+ path + ',' + str(hash_hex) + ',' + str(hop_num) + ',' + str(ping_num) + ',' + min + ',' + avg + ',' + max + ',' + std + ',' + scheme + ',' + str(totalmin) + ',' + str(totalavg) + ',' + str(totalmax) + ','+ str(totalstd) + '\n')
    file6.close()


#############################################################################################
#############################################################################################
#####################  Fetching the Local and Remote IP addressn ############################
def fetch():
    loginToPLC()
    fullSiteList = fetchNorNetSiteList(False)
    localSite    = None
    try:
        localSite = fullSiteList[getLocalSiteIndex()]
    except:
        error('Cannot find local site ' + str(getLocalSiteIndex()) + ' at PLC database!')
    l=dict()
    r=dict()
    localSiteIndex = localSite['site_index']
#    print localSiteIndex
    localProviderList = getNorNetProvidersForSite(localSite)
    l[localSiteIndex]={}
    for localProviderIndex in localProviderList:
        localProvider = localProviderList[localProviderIndex]
        l[localSiteIndex][localProviderIndex]={}
        for version in [ 4, 6 ]:
            localProviderNetwork = makeNorNetIP(localProviderIndex, localSiteIndex, 0, version)
	    tunnelboxAddress = makeNorNetIP(localProviderIndex, localSiteIndex, NorNet_NodeIndex_Tunnelbox, version)
#            print str(tunnelboxAddress.ip)
            if version == 4 :
                localpublicip4 = localProvider['provider_tunnelbox_ipv4'].ip
#	        print "Local-TunnelBox-Private-IP4: " + str(tunnelboxAddress.ip) + "     Local-TunnelBox-Public-IP4: " + str(localpublicip4)
	        l[localSiteIndex][localProviderIndex][4]=localpublicip4
            if version == 6 :
                localpublicip6 = localProvider['provider_tunnelbox_ipv6'].ip
#	        print "Local-TunnelBox-Private-IP6: " + str(tunnelboxAddress.ip) + "     Local-TunnelBox-Public-IP6: " + str(localpublicip6)
                l[localSiteIndex][localProviderIndex][6]=localpublicip6
    r[localSiteIndex]={}
    for remoteSiteIndex in fullSiteList:
        remoteSite = fullSiteList[remoteSiteIndex]
        if remoteSite['site_enabled'] == False:
            continue
        if remoteSiteIndex == localSiteIndex:
            continue
        r[localSiteIndex][remoteSiteIndex]={}
        remoteProviderList = getNorNetProvidersForSite(remoteSite)
        r[localSiteIndex][remoteSiteIndex][4]={}
        r[localSiteIndex][remoteSiteIndex][6]={}
        for remoteProviderIndex in remoteProviderList:
            remoteProvider = remoteProviderList[remoteProviderIndex]
            remoteProviderList = getNorNetProvidersForSite(remoteSite)
            address = makeNorNetIP(remoteProviderIndex, remoteSiteIndex, NorNet_NodeIndex_Tunnelbox, version)
            remotepublicip4 = remoteProvider['provider_tunnelbox_ipv4'].ip
#            print "Remote-TunnelBox-Private-IP4-SiteIndex " + str(remoteSiteIndex) + ":  " + str(address.ip) + "     Remote-TunnelBox-Public_IP4-SiteIndex " + str(remoteSiteIndex) + ":  " + str(remotepublicip4)
            r[localSiteIndex][remoteSiteIndex][4][remoteProviderIndex]=remotepublicip4
            remotepublicip6 = remoteProvider['provider_tunnelbox_ipv6'].ip
            if str(remotepublicip6) != "::" :
                r[localSiteIndex][remoteSiteIndex][6][remoteProviderIndex]=remotepublicip6
#                print "Remote-TunnelBox-Private-IP6-SiteIndex " + str(remoteSiteIndex) + ":  " + str(address.ip) + "     Remote-TunnelBox-Public_IP6-SiteIndex " + str(remoteSiteIndex) + ":  " + str(remotepublicip6)
    return(l,r)


#########################################################################################
####################### Job4 #############################################################
def job4(localip,remotelist,localsiteindex,localproviderindex):
    for localsiteindex in remotelist:
        for remotesiteindex in remotelist[localsiteindex]:
            for version in remotelist[localsiteindex][remotesiteindex]:
                if ( version == 4):
                    for remoteproviderindex in remotelist[localsiteindex][remotesiteindex][version]:
                        remoteip = remotelist[localsiteindex][remotesiteindex][version][remoteproviderindex]
                        index = str(remotesiteindex) + "-" + str(localproviderindex) + "-" + str(remoteproviderindex)
                        filename = "route4-" + index
                        traceroute4(localip,remoteip,localsiteindex,localproviderindex,remotesiteindex,remoteproviderindex,filename)
   # print '[%s] => %s' % (os.getpid(),localip)
        



#########################################################################################
####################### Job6 #############################################################
def job6(localip,remotelist,localsiteindex,localproviderindex):
    for localsiteindex in remotelist:
        for remotesiteindex in remotelist[localsiteindex]:
            for version in remotelist[localsiteindex][remotesiteindex]:
                if ( version == 6):
                    for remoteproviderindex in remotelist[localsiteindex][remotesiteindex][version]:
                        remoteip = remotelist[localsiteindex][remotesiteindex][version][remoteproviderindex]
                        index = str(remotesiteindex) + "-" + str(localproviderindex) + "-" + str(remoteproviderindex)
                        filename = "route6-" + index
                        traceroute6(localip,remoteip,localsiteindex,localproviderindex,remotesiteindex,remoteproviderindex,filename)
   # print '[%s] => %s' % (os.getpid(),localip)


##########################################################################################
##########################################################################################
############################# main Section ###############################################

conf_path="/etc/nornet/Trace-Configuration"
current_dir = str(os.getcwd())
l = dict()
r = dict()
now = datetime.datetime.now()
first_hour = now.hour
(l,r) = fetch()
conf_file = open(conf_path,'r')                              
for line in conf_file :
    line = str(line)
    if ((line.split(" ")[0]) == "result_path"):
        result_path = str(line.split(" ")[2])
while True:
    now = datetime.datetime.now()
    hour = now.hour
    second_hour = first_hour + 12
    if (second_hour >= 24): 
        if ((second_hour - 24) == hour) :
            first_hour = hour
            (l,r) = fetch()
            conf_file = open(conf_path,'r')
            for line in conf_file :
                line = str(line)
                if ((line.split(" ")[0]) == "result_path"):
                    result_path = str(line.split(" ")[2])
    else:
        if (second_hour == hour) :
            first_hour = hour  
            (l,r) = fetch()
            conf_file = open(conf_path,'r')                              
            for line in conf_file :
                line = str(line)
                if ((line.split(" ")[0]) == "result_path"):
                    result_path = str(line.split(" ")[2])
    rd, wrt = os.pipe()
    children = []
    for localsiteindex in l:
        for localproviderindex in l[localsiteindex]:
            for version in l[localsiteindex][localproviderindex]:
                child = os.fork()
                if child == 0 :
                    if (version == 4):
                        localip4=l[localsiteindex][localproviderindex][version]
                        job4(localip4,r,localsiteindex,localproviderindex)
#                        print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
                        os._exit(0)
                    if (version == 6):
                        localip6=l[localsiteindex][localproviderindex][version]
                        if (str(localip6) != '::'):
                            job6(localip6,r,localsiteindex,localproviderindex)
                            print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
                        os._exit(0)
                else:
                    children.append(child)
    for child in children:
        child , status = os.waitpid(child,0)
       # if os.WIFEXITED(status):
           # print "parent: child with child %d exited with value %d" %(child , os.WEXITSTATUS(status))
    #########################################################################################

    if not os.path.exists(result_path):
        os.makedirs(result_path)
    list = os.listdir(str(current_dir))
    for data in list:
        if str(data).startswith("route"):
            cpcm = "mv " + str(current_dir) + "/" + str(data) + " " + result_path
            try:
                comm = subprocess.call(cpcm , shell = True)
            except:
                sys.exit()

        
    time.sleep(120)

